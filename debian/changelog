r-bioc-cummerbund (2.48.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 14 Jan 2025 12:30:32 +0100

r-bioc-cummerbund (2.48.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 03 Dec 2024 18:47:47 +0100

r-bioc-cummerbund (2.46.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:56:09 +0200

r-bioc-cummerbund (2.46.0-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * d/tests/autopkgtest-pkg-r.conf: skip on 32-bit architectures.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 14:59:44 +0200

r-bioc-cummerbund (2.44.0-1) unstable; urgency=medium

  * New upstream version
  * Add d/t/autopkgtest-pkg-r.conf

 -- Andreas Tille <tille@debian.org>  Fri, 08 Dec 2023 16:11:20 +0100

r-bioc-cummerbund (2.42.0-2) unstable; urgency=medium

  * Drop debian/gbp.conf
  * Upstream does not provide any test
  * No need to install vignettes as examples

 -- Andreas Tille <tille@debian.org>  Sat, 05 Aug 2023 07:19:45 +0200

r-bioc-cummerbund (2.42.0-1) unstable; urgency=medium

  * Disable reprotest
  * Provide autopkgtest-pkg-r.conf to make sure runit will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r
  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 22:44:54 +0200

r-bioc-cummerbund (2.40.0-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 22 Nov 2022 14:35:14 +0100

r-bioc-cummerbund (2.38.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 17 May 2022 10:00:52 +0200

r-bioc-cummerbund (2.36.0-1) unstable; urgency=medium

  * Team upload.
  * [9f8382e] New upstream version 2.36.0

 -- Nilesh Patra <nilesh@debian.org>  Fri, 26 Nov 2021 20:06:37 +0530

r-bioc-cummerbund (2.34.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)

 -- Andreas Tille <tille@debian.org>  Wed, 15 Sep 2021 06:53:10 +0200

r-bioc-cummerbund (2.32.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Andreas Tille <tille@debian.org>  Tue, 03 Nov 2020 17:30:01 +0100

r-bioc-cummerbund (2.30.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Testsuite: autopkgtest-pkg-r (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Fri, 22 May 2020 19:48:40 +0200

r-bioc-cummerbund (2.28.0-1) unstable; urgency=medium

  * New upstream version
  * Fixed debian/watch for BioConductor
  * debhelper-compat 12
  * Standards-Version: 4.4.1
  * autopkgtest: s/ADTTMP/AUTOPKGTEST_TMP/g
  * Remove obsolete fields Name from debian/upstream/metadata.

 -- Andreas Tille <tille@debian.org>  Wed, 13 Nov 2019 09:54:27 +0100

r-bioc-cummerbund (2.26.0-1) unstable; urgency=medium

  * New upstream version
  * debhelper 12
  * Standards-Version: 4.4.0
  * Remove trailing whitespace in debian/copyright

 -- Andreas Tille <tille@debian.org>  Fri, 19 Jul 2019 17:04:01 +0200

r-bioc-cummerbund (2.24.0-2) unstable; urgency=medium

  * Team upload
  * Do not call sessionInfo() while building vignettes,
    it fails on low-memory autopkgtesters

 -- Graham Inggs <ginggs@debian.org>  Wed, 05 Dec 2018 12:02:13 +0000

r-bioc-cummerbund (2.24.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.2.1

 -- Dylan Aïssi <daissi@debian.org>  Sun, 11 Nov 2018 10:36:53 +0100

r-bioc-cummerbund (2.22.0-1) unstable; urgency=medium

  * Rebuild for R 3.5 transition
  * New upstream version
  * debhelper 11
  * Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-
    lists.debian.net>
  * Point Vcs fields to salsa.debian.org
  * dh-update-R to update Build-Depends

 -- Andreas Tille <tille@debian.org>  Thu, 07 Jun 2018 13:20:32 +0200

r-bioc-cummerbund (2.20.0-1) unstable; urgency=medium

  * New upstream version
  * Add Registry entry

 -- Andreas Tille <tille@debian.org>  Thu, 09 Nov 2017 11:08:21 +0100

r-bioc-cummerbund (2.18.0-1) unstable; urgency=medium

  * New upstream version
  * Secure URI in watch file
  * Standards-Version: 4.1.1
  * debhelper 10
  * Add debian/README.source to document binary data files

 -- Andreas Tille <tille@debian.org>  Sun, 22 Oct 2017 18:34:39 +0200

r-bioc-cummerbund (2.16.0-3) unstable; urgency=medium

  * Team upload.
  * Rebuild for r-api-3.4 transition.

 -- Sébastien Villemot <sebastien@debian.org>  Tue, 03 Oct 2017 23:04:00 +0200

r-bioc-cummerbund (2.16.0-2) unstable; urgency=medium

  * Build-Depends: r-cran-foreign, r-cran-nnet
    Closes: #842914

 -- Andreas Tille <tille@debian.org>  Wed, 02 Nov 2016 14:09:24 +0100

r-bioc-cummerbund (2.16.0-1) unstable; urgency=medium

  * New upstream version
  * Convert to dh-r
  * Generic BioConductor homepage

 -- Andreas Tille <tille@debian.org>  Thu, 27 Oct 2016 15:53:32 +0200

r-bioc-cummerbund (2.14.0-2) unstable; urgency=medium

  * Fixed autopkgtest (thanks to Gordon Ball)

 -- Andreas Tille <tille@debian.org>  Sun, 03 Jul 2016 08:27:17 +0200

r-bioc-cummerbund (2.14.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 28 Jun 2016 09:24:47 +0200

r-bioc-cummerbund (2.12.1-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * (hopefully) fix autopkgtest

 -- Andreas Tille <tille@debian.org>  Wed, 27 Apr 2016 20:18:32 +0200

r-bioc-cummerbund (2.12.0-1) unstable; urgency=medium

  * New upstream version
    Closes: #803567

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2015 15:20:28 +0100

r-bioc-cummerbund (2.10.0-1) unstable; urgency=medium

  * New upstream version
  * cme fix dpkg-control
  * Update dependencies
  * Update copyright info
  * Add autopkgtest based on vignettes

 -- Andreas Tille <tille@debian.org>  Sun, 28 Jun 2015 21:57:11 +0200

r-bioc-cummerbund (2.6.1-1) unstable; urgency=low

  * New upstream version
    Closes: #753193
  * Drop Carlos Borroto from Uploaders at his request
  * Moved debian/upstream to debian/upstream/metadata

 -- Andreas Tille <tille@debian.org>  Wed, 30 Jul 2014 21:40:29 +0200

r-bioc-cummerbund (2.4.1-1) unstable; urgency=low

  * New upstream version
  * debian/rules: Remove useless creation of ${R-Depends}
  * debian/control: Versioned Build-Depends: r-base-dev (>= 3.0)
  * debian/README.test: add hint how to test the package

 -- Andreas Tille <tille@debian.org>  Sat, 28 Dec 2013 17:17:25 +0100

r-bioc-cummerbund (1.2.0-2) unstable; urgency=low

  [ Charles Plessy ]
  f419ef1 Normalised VCS URLs following Lintian's standard.
  d5c4363 Removed obsolete DM-Upload-Allowed field.
  6982298 Normalised Debian copyright file with 'cme fix dpkg-copyright'.
  38bee3c Normalised debian/control with 'cme fix dpkg-copyright'.
  9587f21 Use Debhelper 9.
  9560362 Conforms with Policy 3.9.4.

  [ Andreas Tille ]
  * Upload to unstable after verifying that predepends are properly
    build

 -- Andreas Tille <tille@debian.org>  Sat, 18 May 2013 14:30:43 +0200

r-bioc-cummerbund (1.2.0-1) unstable; urgency=low

  * New upstream release
  * debian/watch: Track stable releases instead of development releases
  * debian/upstream: Reference as given in
      citation("cummeRbund")
  * debian/control: Standards-Version: 3.9.3 (no changes needed)
  * debian/rules: Enable hardening

 -- Andreas Tille <tille@debian.org>  Tue, 08 May 2012 08:52:06 +0200

r-bioc-cummerbund (1.1.3-1) unstable; urgency=low

  * Initial release (Closes: #657997)

 -- Carlos Borroto <carlos.borroto@gmail.com>  Tue, 24 Jan 2012 12:08:29 -0500
